class InterleaveStrings
{
    public static String interleave(String s1, String s2)
    {
        String s = "";

        for (int i = 0; i < s1.length(); i++) {
            s += Character.toString(s1.charAt(i)) +
                     Character.toString(s2.charAt(i));
        }

        return s;
    }

    public static void main(String[] args)
    {
        if (args.length == 2)
        {
            System.out.println(interleave(args[0], args[1]));
        }
        else
        {
            System.out.println("Please enter two command line arguments!");
        }
    }
}

