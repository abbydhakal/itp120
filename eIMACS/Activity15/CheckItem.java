public class CheckItem
{
  private double myPrice,
                 mySalesTax;

  private int    myQuantity = 1;

  public CheckItem( double price, double salesTax )
  {
    myPrice = price;
    mySalesTax = salesTax;
  }

  public int getQuantity()
  {
    return myQuantity; 
  }

  public void setQuantity( int qty )
  {
    myQuantity = qty; 
  } 
  
  // Create your method here
    public double lineItemTotal()
    {
        double notRounded = myPrice * myQuantity * (1 + (mySalesTax / 100));
        return roundMoney(notRounded);
    }
    
    public static double roundMoney( double amount )
    {
        return (int)(100 * amount + 0.5) / 100.0;
    } 
}