/**
 *
 * @author Jeff Elkner
 * @version 1.0 2019-10-24
 */ 
import java.awt.Graphics;
import java.awt.Color;
import java.lang.Math;

public class APRectangle
{
    private APPoint myTopLeft;
    private double  myWidth;
    private double  myHeight;
    private double myAngleDeg = 0;
    private double myAngleRad = 0;
    private Color myColor = new Color(0, 0, 0);

    public APRectangle( APPoint topLeft, double width, double height )
    {
        // Insert code to initialize the instance variables here
        myTopLeft = topLeft;
        myWidth = width;
        myHeight = height;
    }
    public APRectangle(APPoint topLeft, double width, double height, double angle)
    {
        this(topLeft, width, height);
        myAngleDeg = angle;
        myAngleRad = Math.toRadians(angle);
    }
    public APRectangle(APPoint topLeft, double width, double height, int[] color)
    {
        this(topLeft, width, height);
        myColor = new Color(color[0], color[1], color[2]);
    }
    public APRectangle(APPoint topLeft, double width, double height, double angle, int[] color)
    {
        this(topLeft, width, height, angle);
        myColor = new Color(color[0], color[1], color[2]);
    }
    public APRectangle(APPoint topLeft, double width, double height, int r, int g, int b)
    {
        this(topLeft, width, height, new int[] {r, g, b});
    }
    public APRectangle(APPoint topLeft, double width, double height, double angle, int r, int g, int b)
    {
        this(topLeft, width, height, angle, new int[] {r, g, b});
    }

    public APPoint getTopLeft() {return myTopLeft;}
    public double getWidth() {return myWidth;}
    public double getHeight() {return myHeight;}
    public double getAngleDeg() {return myAngleDeg;}
    public double getAngleRad() {return myAngleRad;}

    public void setTopLeft(APPoint x) {myTopLeft = x;}
    public void setWidth(double x) {myWidth = x;}
    public void setHeight(double x) {myHeight = x;}
    public void setAngleDeg(double x) {myAngleDeg = x; myAngleRad = Math.toRadians(x);}
    public void setAngleRad(double x) {myAngleRad = x; myAngleDeg = Math.toDegrees(x);}

    public APPoint getTopRight()
    {
        double x = myWidth * Math.cos( myAngleRad );
        double y = myWidth * Math.sin( myAngleRad );

        return new APPoint( myTopLeft.getX() + x,
                myTopLeft.getY() - y );
    }
    public APPoint getBottomRight()
    {
        APPoint tr = getTopRight();

        double x = myHeight * Math.sin( myAngleRad );
        double y = myHeight * Math.cos( myAngleRad );

        return new APPoint( tr.getX() + x,
                tr.getY() + y );
    }
    public APPoint getBottomLeft()
    {
        double x = myTopLeft.getX() + (getBottomRight().getX() - getTopRight().getX());
        double y = myTopLeft.getY() - (getTopRight().getY() - getBottomRight().getY());
        return new APPoint(x, y);
    }
    public void shrink(double x)
    {
        myWidth *= (x / 100);
        myHeight *= (x / 100);
    }

    public static String printAPRectangle(APRectangle x)
    {
        return "[APRectangle " + APPoint.printAPPoint(x.getTopLeft()) + " " + x.getWidth() + "," + x.getHeight() + "]";
    }

    public void draw( Graphics g )
    {
        g.setColor(myColor);
        APPoint topLeft = myTopLeft;
        APPoint topRight = getTopRight();
        APPoint bottomLeft = getBottomLeft();
        APPoint bottomRight = getBottomRight();

        g.drawLine( (int)topLeft.getX(), (int)topLeft.getY(), (int)topRight.getX(), (int)topRight.getY() );
        g.drawLine( (int)bottomLeft.getX(), (int)bottomLeft.getY(), (int)bottomRight.getX(), (int)bottomRight.getY() );
        g.drawLine( (int)topLeft.getX(), (int)topLeft.getY(), (int)bottomLeft.getX(), (int)bottomLeft.getY() );
        g.drawLine( (int)bottomRight.getX(), (int)bottomRight.getY(), (int)topRight.getX(), (int)topRight.getY() );
    }
}
