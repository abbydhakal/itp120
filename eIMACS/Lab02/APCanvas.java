/**
 *
 * @author jelkner
 */
 

import java.awt.Graphics;
import javax.swing.JPanel;
//import APRectangle.*;
//import APPoint.*;

/**
 * A panel on which to draw
 *
 * @author |your name|
 * @version 1.0 |today's date|
 */
public class APCanvas extends JPanel
{
    /**
     * The class constructor
     */
    public APCanvas()
    {
    }

    /**
     * Draws on a Graphics object
     *
     * @param g  a Graphics object
     */
    private void paintMe( Graphics g )
    {
        APRectangle r = new APRectangle(new APPoint(200, 200), 50, 50);
        r.draw(g);
        for (int i = 0 ; i < 36 ; i++)
        {
            r.shrink(105);
            r.setAngleDeg(r.getAngleDeg() + 10);
            r.draw(g);
        }
    }

    /**
     * Overrides JPanel's paintComponent method
     *
     * @param g  a Graphics object
     */
    public void paintComponent( Graphics g )
    {
        super.paintComponent( g );
        paintMe( g );
    }
}
