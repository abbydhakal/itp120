public class CardRunner
{ 
    public static void main( String[] args)
    {
          Card[] deck = new Card[52];
          
          for (int s = 0; s < 4; s++)
              for (int r = 0; r < 13; r++)
                  deck[s * 13 + r] = new Card(s, r);
          
          
          for ( Card card : deck ) 
              System.out.println(card);
    }
}