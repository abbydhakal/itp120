 public class Card
  {
    private String[] suits = {"Spades" , "Hearts" , "Diamonds" , "Clubs" };
    private String[] ranks = {"Deuce", "Three", "Four", "Five","Six", "Seven", 
                             "Eight", "Nine", "Ten", "Jack", "Queen", "King", "Ace"};

    private int mySuit;
    private int myRank;

    public Card( int suit, int rank )
    {
      mySuit = suit;
      myRank = rank;
    }
    
    public int getSuit()
    {
        return mySuit;
    }
    
    public int getRank()
    {
        return myRank;
    }
    
    public String toString()
    {
      return ranks[myRank] + " of " + suits[mySuit];
    }
  } 
