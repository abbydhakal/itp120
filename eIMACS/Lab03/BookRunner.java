 public class BookRunner
{
    public static void main( String[] args )
    {
        Book[] library =
        {
          new Book( "David Copperfield", "Charles Dickens" ),
          new Book( "The Jungle Book", "Rudyard Kipling" ),
          new Book( "The Call of the Wild", "Jack London" ),
          new Book( "Frankenstein", "Mary Shelley" ),
          new Book( "Dracula", "Bram Stoker" ),
          new Book( "Anna Karenina", "Leo Tolstoy" )
        };
     
        Book longest = new Book("", "");
        for (Book novel : library)
        {
            if (novel.getTitle().length() > longest.getTitle().length())
                longest = novel;
        }
        System.out.println(longest.getAuthor());
    }
}
