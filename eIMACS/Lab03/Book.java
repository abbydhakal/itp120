public class Book
{
    private String myAuthor;
    private String myTitle;
    
    public Book(String title, String author)
    {
        myAuthor = author;
        myTitle = title;
    }
    
    public String getAuthor(){return myAuthor;}
    public String getTitle(){return myTitle;}
}
