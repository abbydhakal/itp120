import java.util.Arrays;

class SwapTester
{
    public static int[] arrStringToInt(String[] sArr)
    {
        int[] iArr = new int[sArr.length];

        for (int i = 0; i < sArr.length; i++)
        {
            iArr[i] = Integer.parseInt(sArr[i]);
        }

        return iArr;
    }

    public static int[] Swap1(int[] nums)
    //Swap1 occurs here!
    {
        // https://howtodoinjava.com/array/array-copy/
        int[] a = Arrays.copyOf(nums, nums.length);

        //we can avoid iteration over elements using Arrays.copyOf
        a[0] = a[1];
        a[1] = a[0];

        return a;
    }

    public static int[] Swap2(int[] nums)
    //Swap2 occurs here!
    {
        int[] a = Arrays.copyOf(nums, nums.length);

        int t = a[0];
        a[0] = a[1];
        a[1] = t;
        return a;
    }

    public static int[] Swap3(int[] nums)
    //Swap3 occurs here!
    {
        int[] a = Arrays.copyOf(nums, nums.length);

        a[0] = a[0] - a[1];
        a[1] = a[0] + a[1];
        a[0] = a[1] - a[0];
        return a;
    }

    public static void main(String[] args)
    {
        int[] intArgs = arrStringToInt(args);
        System.out.println(Arrays.toString(Swap1(intArgs)));
        System.out.println(Arrays.toString(Swap2 (intArgs)));
        System.out.println(Arrays.toString(Swap3 (intArgs)));
    }
}

