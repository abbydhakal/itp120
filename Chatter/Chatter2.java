class Chatter2 {
    public static boolean containsAnyOf(String s, String [] sArr) {
        for (String sElem : sArr) {
            if (s.indexOf(sElem) != -1)
                return true;
        }
        return false;
    }

    public static String getResponse(String s) {
        String[] family = {"mother", "father", "brother", "sister"};
        String[] pets = {"cat", "dog", "parrot", "lizard"};

        if (s.indexOf("no") != -1)
            return "Why so negative?";
        if (containsAnyOf(s, family))
            return "Tell me more about your family.";
        if (containsAnyOf(s, pets))
            return "Tell me more about your pets.";
        return "Interesting. Tell me more.";
    }

    public static void main(String[] args) {
        System.out.println(getResponse(args[0]));
    }
}
