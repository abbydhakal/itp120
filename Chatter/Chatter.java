class Chatter {
    public static String getResponse(String s) {
        String[] family = {"mother", "father", "brother"};
        
    if (s.indexOf("no") != -1)
        return "Why so negative?";
    for (String member : family){
        if (s.indexOf(member) !=-1)
            return "Tell me more about your family.";
    }

    return "Interesting. Tell me more.";
    }

    public static void main(String[] args) {
        System.out.println(getResponse(args[0]));
    }
}

