import java.util.Scanner;
  
class XCubedTest3
{
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n;

        System.out.print("Enter an integer number: ");

        n = scan.nextInt();

        System.out.println("Cube of " + n + " is: " + Math.pow(n, 3));
        
        System.out.println("Cube of " + n + " plus five is: " + (System.out));
    }

}
