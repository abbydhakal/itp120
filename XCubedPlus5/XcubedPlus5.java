class XcubedPlus5      
{
    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);

        System.out.println((int)Math.pow(n, 3) + 5);
    }
}
