import java.util.Scanner;

class XCubedTest2
{ 
    public static void main(String[] args) {
	Scanner scan = new Scanner(System.in);
        int n;

	System.out.print("Enter an integer number: ");
	
        n = scan.nextInt();
        System.out.println("Cube of " + n + " plus 5 is: " + (Math.pow(n, 3) + 5));

    }

}
