public class Counter 
  { 
    private int myN; 
    {
        int t = 0; 
        int i; 
        Counter c = new Counter( 10 ); 
        for ( i = 0 ; i < 5 ; i++ ) 
    {      
        c = c.nextCounter(); 
        t = c.getN(); 
    }
    } 

    public Counter( int n ) 
    { 
      myN = n; 
    } 

    public Counter nextCounter() 
    { 
      return new Counter( myN + 1 ); 
    } 

    public int getN() 
    { 
      return myN;  
    } 
  }
