
public class Time
{
    private int hour;
    private int minute;
    private int second;
    
    public Time()
    {
        myHour = 0;
        myMinute = 0;
        mySecond = 0;
    }
    
    public Time(int hour, int minute, int second)
    {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }
    
    public String toString()
    {
        return Time(this.hour, this.minute, this.second);
    }
    
    public void setHour(int hour)
    {
        this.hour = hour;
    }
    
    public void setMinute(int minute)
    {
        this.minute = minute;
    }
    
    public void setSecond(int second)
    {
        this.second = second;
    }
    
    public Time addTime(Time t)
    {
        int newHour = 0;
        int newMinute = 0;
        int newSecond = 0;
        
        if ((this.second + t.getSecond()) >= 60)
            newMinute += 1;

        if ((this.minute + t.getMinute()) >= 60)
            newHour += 1;

        newHour = (this.hour + t.getHour()) % 60;
        newMinute = (this.minute + t.getMinute()) % 60;
        newSecond = (this.second + t.getSecond()) % 60;

        return Time(newHour, newMinute, newSecond);
    }
}