public class Time
{
    private int hour;
    private int minute;
    private int second;
    
    public Time()
    {
        aHour = 0;
        aMinute = 0;
        aSecond = 0;
        ///initilizing these variables by assigning them zero
    }
    
    public Time(int hour, int minute, int second)
    {
        aHour = hour;
        aMinute = minute;
        aSecond = second;
    }
    
    public int getHour()
    {
        return ahour;
    }
    
    public void setHour(int h)
    {
        aHour = h;
    }
    
    public int getMinute()
    {
        return aMinute;
    }
    
    public void setMinute(int m)
    {
        aMinute = m;
    }
    
    public int getSecond()
    {
        return aSecond;
    }
    
    public void setSecond(int s)
    {
        aSecond = s;
    }
    
    public String toString()
    {
        return Time(aHour, aMinute, aSecond);
    }
    
    public Time addTime(Time t)
    {
        int addSeconds = aSecond + t.getSecond();
        int addMinutes = aMinute + t.getMinute() + addSeconds / 60;

        return new Time(
            aHour + t.getHour() + addMinutes / 60,
            addMinutes % 60,
            addSeconds % 60);

    }
}