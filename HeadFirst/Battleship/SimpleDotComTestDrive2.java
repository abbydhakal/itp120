public class SimpleDotComTestDrive2
{
    public static void main (String [] args)
    {
        SimpleDotCom2 dot = new SimpleDotCom2();
        //instantiate a SimpleDotCom object
        
        int [] locations = {2, 3, 4};
        dot.setLocationCells(locations);
        //locations invokes the setter method on the dot com
        
        String userGuess = "2";
        // make a fake user guess
        
        String result = dot.checkYourself(userGuess);
        String testResult = "failed";
        
        if (result.equals("hit"))
        {
            testResult = "passed";
        }
        
        System.out.println(testResult);
   }
}