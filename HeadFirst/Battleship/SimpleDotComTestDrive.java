public class SimpleDotComTestDrive
{ 
    public static void main(String[] args)
    {
        SimpleDotCom dot = new SimpleDotCom();
        String userGuess;
        String result;

        int[] locations = {3, 4, 5};
        dot.setLocationCells(locations);

        assert dot.numOfHits() == 0: "Expected numOfHits to be 0";
        // hit count for the new hit
        // getter
        
        userGuess = "2";
        result = dot.checkYourself(userGuess);
        assert result == "miss" : "Expected miss, got " + result;

        userGuess = "3";
        result = dot.checkYourself(userGuess);
        //assert result == "hit" : "Expected hit, got " + result;
        
        
        userGuess = "4";
        result = dot.checkYourself(userGuess);
        assert result == "kill" : "Expected kill, got " + result;
        
        //assert dot.numOfHits() == 3: "Expected hitCount to be 3";

        // need an assertion for when hit is 1
        // need

        System.out.println(result);
    }
}
