public class SimpleDotCom2
{
    private int [] locationCells = {2, 3, 4};
    private int numOfHits = 0;
    
    
    public String checkYourself(String userGuess)
    {
        int guess = Integer.parseInt(userGuess);
        //converts string into an int
        String result = "miss";
        //variable that holds the result for when you miss
        
        for (int cell : locationCells)
        //repeat for each element in the locationCells array
        //take the next element in the array and assign it to cell
        
        {
            if (guess == cell)
            {
                result = "hit";
                numOfHits = numOfHits + 1;
                break;
                //stops the loop
            }
        }
        
        if (numOfHits == locationCells.length)
        {
            result = "kill";
        }
        
        System.out.println(result);
        //tells the user if they missed, hit, or killed a ship
        
        return result;
        //returns the result back to the method checkYourself */
    }
    
    public void setLocationCells(int[] loc)
    {
        locationCells = loc;
    }
}