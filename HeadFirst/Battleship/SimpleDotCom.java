public class SimpleDotCom
{
    int [] locationCells;
    public int numOfHits = 0;
    //work on keeping track of the number of hits
    
    public void setLocationCells(int[] locs)
    {
        locationCells = locs;
    }
    public int numOfHits()
    {
        return 0;
    }
    public String checkYourself(String userGuess)
    {
        int guess = Integer.parseInt(userGuess);
        String result = "miss";
        
        for (int cell : locationCells)
        {
            if (cell == guess)
            {
                numOfHits ++;
                result = "hit";
                // System.out.print(result);
                // return result;
            }
        }
        if (numOfHits == locationCells.length)
        {
            result = "kill";
            System.out.println(result);
            return result;
        }
        
        System.out.println(result);
        
        return result;
        //returns the result back to the method checkYourself
    }
}