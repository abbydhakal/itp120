/* public class SimpleDotComGame
{
    private int[] locations = {1, 2, 3}
    private numHits = 0;
    private numGuesses = 0;
    
    public boolean over()
    {
        return numHits == 3;
    }
    
    public makeMove(String userGuess)
    {
        numGuesses ++;
        int guess = Integer.parseInt(userGuess); 
        for (int i : locations)
        {    
            if (i == guess)
            {
                numHits ++;
                if (numHits == 3)
                    return "kill";
                return "hit";
            }
        }
        return "miss";
    }

    public String displayResults()
    {
        return "It took you " + numGuesses + " guesses.";
    }
}*/

public class SimpleDotComGame
{
    private int[] locations = {1, 2, 3};
    private int numHits = 0;
    private int numGuesses = 0;

    public boolean over()
    {
        return numHits == 3;
    }

    public String makeMove(String userGuess)
    {
        numGuesses ++;
        int guess = Integer.parseInt(userGuess);

        for (int i : locations)
        {
            if (i == guess)
            {
                numHits ++;
                if (numHits == 3)
                    return "kill";
                return "hit";
            }
        }
        return "miss";
    }

    public String displayResults()
    {
        return "It took you " + numGuesses + " guesses";
    }
}
