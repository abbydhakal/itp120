class PersonTester
{
    public static void main(String[] args)
    {
        if (args.length!= 2)
        {
            System.out.println("Please enter two command line arguments!");
        }
        
        else
        {
            Person somebody = new Person(args[0], Integer.parseInt(args[1]));
            System.out.println(somebody);

        }
    }
}

