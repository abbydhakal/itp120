public class Person 
/* by making this a public class we ensure that instances of the class are visiblefrom anywhere in our program */
{
    private double myHeight;
    private double myWeight;
    private String myHair;

    /* by declaring these instances as private we can tell java that each instance of the Person class is to have its own instance variables that it alone may access */

    public Person(double height, double weight, String hair)
    /* every class definition must have a constructor that has the same name as the class. The public access modifier tells Java that the constructor is avaliable to all of our code */
    //constructors have no return data type (not even void)
    //the above line with the three parameters is called the constructor signature
    {
        myHeight = height;
        myWeight = weight;
        myHair = hair;
        // myHeight-Weight-Hair are three formal parameters
    }
    
    public void sleep(int hours)
    {
        for (int i = 0; i < hours; i++)
            System.out.println("Sleeping...");
    }

    public void talk()
    {
        myWeight -= 0.01;
        System.out.println("Talking...");
    }

    public String getHair()
    {
        return myHair;
    }
}

public static void main(String[] args)
{
    Person eric = new Person( 1.57, 67.2, "brown" );
    for ( int i = 0 ; i < 5 ; i++ )
      eric.talk();

    /* System.out.println( "Eric now weighs " + eric.getWeight() + "kg" ); 
    Person eric = new Person(1.57, 67.2, "brown");
    Person jenny = new Person(1.55, 56.8, "blond"); */

    /* System.out.println("Eric's hair is " + eric.getHair() );
    System.out.println("Jenny's hair is " + jenny.getHair() );
    eric = new Person(1.57, 67.2, "brown"); */

}
