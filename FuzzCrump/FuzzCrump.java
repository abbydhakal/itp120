import java.util.Random;

public class FuzzCrump
{
    private int age;
    private int guessCount;
    private String name; 

    public FuzzCrump(String someName)
    {
        name = someName;
        age = (int)(Math.random() * 10) + 10;
        //random usually goes from 0 to 1
    }
    public String sayHi()
    {
        return "My name is " + name + " and I just want to say, " + 
            "that the thing I like most is to play, play, play, play!";
    }

    public String getMyName()
    {
        return name;
    }

    public String howOld(int guess)
    {
        guessCount++;
        if (guess > age)
            return "I'm not that old!";
        if (guess < age)
            return "I'm older than that!";
        return "You guessed my age!";
        //where all of the comparing happens
        //guessCount++;
    }

    public String getGuessCount()
    {
        return "It took you " + guessCount + " guesses!";
    }
}

